# [NetworkManager VPN Plugin: Wireguard](https://github.com/max-moser/network-manager-wireguard) Packages

## Supported Ubuntu distributions

* bionic
* focal

### Add repo signing key to apt

```bash
wget -q -O - https://packaging.gitlab.io/network-manager-wireguard/gpg.key | sudo apt-key add -
```

or

```bash
curl -sL https://packaging.gitlab.io/network-manager-wireguard/gpg.key | sudo apt-key add - 
```

### Add repo to apt

#### Bionic

```bash
REPO=network-manager-wireguard-bionic; echo "deb [arch=amd64] https://packaging.gitlab.io/${REPO%-*} ${REPO##*-} main" | sudo tee /etc/apt/sources.list.d/morph027-${REPO}.list
```

#### Focal

```bash
REPO=network-manager-wireguard-focal; echo "deb [arch=amd64] https://packaging.gitlab.io/${REPO%-*} ${REPO##*-} main" | sudo tee /etc/apt/sources.list.d/morph027-${REPO}.list
```
